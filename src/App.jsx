import React from "react";
import IndexPage from "./IndexPage";
import "./assets/scss/app.scss";

export default class App extends React.Component {
  render() {
    
    return (
      <div className="app">
        <IndexPage />
      </div>
    );
  }
}
