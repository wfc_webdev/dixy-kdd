import axios from "axios";

export default axios.create({
    baseURL: "https://base_url", 
    responseType: "json",
    headers: {
        // 'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
    }
});