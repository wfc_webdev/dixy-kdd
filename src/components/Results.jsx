import React, { useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Pagination, Navigation } from 'swiper';

import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';
import { res1, res2, res3, res4 } from './res';

SwiperCore.use([Pagination, Navigation]);

const tabs = [
	{
		date: '20 сентября',
		content: res1
	},
	{
		date: '4 октября',
		content: res2
	},
	{
		date: '18 октября',
		content: res3
	},
	{
		date: '5 ноября',
		content: res4
	}
];

// const WINDOW_WIDTH = window.innerWidth;

const Results = () => {
	const [activeTab, toggleTab] = useState(0);

	let preparedWinners = [];
	let arr = [].concat(tabs[activeTab].content);
	let max = Math.ceil(tabs[activeTab].content.length / 7);
	for (let i = 0; i < max; i++) {
		preparedWinners.push(arr.splice(0, 7));
	}

	console.log(preparedWinners);

	return (
		<div className='results'>
			<div className='container'>
				<h2 className='results__title title text-white text-uppercase text-bold text-centered'>
					результаты розыгрышей
				</h2>

				<div className='slider_content'>
					<div className='slider_content_inner'>
						<div className='tabs'>
							{tabs.map((item, id) => (
								<button
									className={
										'tab button ' +
										(activeTab === id ? 'active' : '')
									}
									key={id}
									onClick={() => toggleTab(id)}>
									{item.date}
								</button>
							))}
						</div>
						<div className='slider_content__data'>
							<Swiper
								pagination={{
									type: 'progressbar'
								}}
								slidesPerView={3}
								slidesPerGroup={3}
								navigation={true}
								breakpoints={{
									320: {
										spaceBetween: 20,
										slidesPerView: 'auto',
										slidesPerGroup: 1
									},
									630: {
										slidesPerView: 2,
										slidesPerGroup: 2
									},
									1024: {
										slidesPerView: 3,
										slidesPerGroup: 3,
										spaceBetween: 30
									},
									1200: {
										spaceBetween: 77
									}
								}}>
								{preparedWinners.map((item, id) => (
									<SwiperSlide key={id}>
										{item.map((winner, index) => {
											return (
												<p
													className={winner.special ? 'winner special' : 'winner'}
													key={`${winner.name}__${index}`}>
													<span className='text-bold'>
														{winner.name
															.charAt(0)
															.toUpperCase() +
															winner.name.slice(
																1
															)}
													</span>
													{winner.phone}
												</p>
											);
										})}
									</SwiperSlide>
								))}
							</Swiper>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Results;
