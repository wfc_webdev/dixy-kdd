import React from 'react';

const Rules = () => {
	return (
		<section className='rules'>
			<div className='container'>
				<h2 className='rules__title text-bold text-orange text-centered text-uppercase title'>
				Правила участия в розыгрыше
				</h2>
				<p className='text text-green text-centered text-bold rules__subtitle'>
					Период акции с 6 сентября 2021г. по 31 октября 2021г.
				</p>

				<div className='rules__items'>
					<div className='rules__item text'>
						<div className='rules__place text-orange text-bold'>
							1
						</div>
						<p className='text-ubuntu text-bold'>
							<span className='rules__item-link'>
								<a href='https://dixy.ru/app/'>Скачивайте</a>
							</span> приложение
							{' '}и вступайте в «Клуб Друзей Дикси»
						</p>
					</div>
					<div className='rules__item text'>
						<div className='rules__place text-orange text-bold'>
							2
						</div>
						<p className='text-ubuntu text-bold'>
							Совершите покупку на сумму 700 ₽
						</p>
					</div>
					<div className='rules__item text'>
						<div className='rules__place text-orange text-bold'>
							3
						</div>
						<p className='text-ubuntu text-bold'>
						Участвуйте в розыгрыше призов. Главный приз - автомобиль!
						</p>
					</div>
				</div>
					<a className='rules__button text-bold button' href='https://dixy.ru/uploads/kdd_reload.pdf' target="_blank">
						Полные правила акции
					</a>
			</div>
		</section>
	);
};

export default Rules;
