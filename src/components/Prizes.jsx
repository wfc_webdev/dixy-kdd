import React from 'react';

import car from '../assets/images/prizes/prizes-car.png';

import wear from '../assets/images/prizes/prizes-wear.png';
import cup from '../assets/images/prizes/prizes-cup.png';
import bag from '../assets/images/prizes/prizes-bag.png';
import coupons from '../assets/images/prizes/prizes-coupons.png';

const Prizes = () => {
	return (
		<div className='prizes'>
			<div className='container'>
				<div className='prizes__wrapper'>
					<h3 className='title text-bold text-orange text-centered text-uppercase prizes__title'>
					Призы
					</h3>
					<div className='prizes__image'>
						<img src={car} alt='Car' />
					</div>
					<p className='prizes__subtitle text-green text-centered text-bold'>
						Главный приз
						<span className='text-uppercase'>
							Автомобиль Hyundai Solaris
						</span>
					</p>
					<div className='prizes__items'>
						<div className='prizes__item'>
							<div className='prizes__item-image'>
								<img src={wear} alt='' />
							</div>
							<div className='prizes__item-name text-green text-bold text-centered'>
								Худи
							</div>
						</div>
						<div className='prizes__item'>
							<div className='prizes__item-image'>
								<img src={cup} alt='' />
							</div>
							<div className='prizes__item-name text-green text-bold text-centered'>
								Термокружка
							</div>
						</div>
						<div className='prizes__item'>
							<div className='prizes__item-image'>
								<img src={bag} alt='' />
							</div>
							<div className='prizes__item-name text-green text-bold text-centered'>
								Рюкзак
							</div>
						</div>
						<div className='prizes__item'>
							<div className='prizes__item-image'>
								<img src={coupons} alt='' />
							</div>
							<div className='prizes__item-name text-green text-bold text-centered'>
								Сертификаты номиналами 200₽, 500₽ и 1000₽
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Prizes;
