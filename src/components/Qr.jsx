import React from 'react';

import qr from '../assets/images/qr.png';
import ios from '../assets/images/ios.png';
import googlePlay from '../assets/images/google-play.png';

const WIDTH = window.innerWidth;

const Qr = () => {
	return (
		<section className='qr'>
			<div className='container'>
				<div className='qr__wrapper'>
					<p className='text-orange text-bold text-uppercase'>
						для участия скачивайте
						<span>наше&nbsp;приложение</span>
						«клуб друзей дикси»
					</p>
					{WIDTH > 500 ? (
						<img src={qr} alt='' />
					) : (
						<div className='qr__links'>
							<a href='https://dixy.ru/app'>
								<img src={ios} alt='' />
							</a>
							<a href='https://dixy.ru/app'>
								<img src={googlePlay} alt='' />
							</a>
						</div>
					)}
				</div>
			</div>
		</section>
	);
};

export default Qr;
