import React from 'react';
import Benefits from './components/Benefits';
import Footer from './components/Footer';
import Header from './components/Header';
import Prizes from './components/Prizes';
import Qr from './components/Qr';
import Results from './components/Results';
import Rules from './components/Rules';

import { Hoc } from './Hoc';

class IndexPage extends React.Component {
	async componentDidMount() {}

	render() {
		return (
			<>
				<Header />
				<Rules />
				<Prizes />
				<Benefits />
				<Qr />
				<Results />
				<Footer />
			</>
		);
	}
}

export default Hoc(IndexPage);
